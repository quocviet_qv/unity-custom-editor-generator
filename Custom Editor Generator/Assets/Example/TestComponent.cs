﻿using System;
using UnityEngine;
using System.Collections;

public class TestComponent : MonoBehaviour
{
	public float publicFloat;

	[NonSerialized]
	public float publicNonSerializedFloat;

	private float privateFloat;

	[SerializeField]
	private float privateSerializedFloat;

	[Range(0,1)]
	public float rangeFloat;

	public TestSerializableData testData;
}

[Serializable]
public class TestSerializableData
{
	public bool enabled;
	public string name;
}