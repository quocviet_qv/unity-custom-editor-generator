# Custom Editor Generator #

Hi custom editor lovers. I made an easy way to generate boilerplate code for MonoBehaviour and ScriptableObject inspector editors. 

### Why? ###

I write custom inspector for 95% of my components. Most of the time i just need a small modifications, which property decorators can't handle. Because of that, i spend most of my time writing boilerplate code for custom inspectors. 

### How do I it? ###

* Right click on your script in Project Window
* Click 'Create -> Editor From Selected Script'
* It will create a subdirectory 'Editor' and create a file inside named 'YourCustomComponentEditor'

![Screenshot 2016-07-30 15.35.58.png](https://bitbucket.org/repo/nLdEon/images/4220662886-Screenshot%202016-07-30%2015.35.58.png)

### Example Component ###

![Screenshot 2016-07-30 15.37.12.png](https://bitbucket.org/repo/nLdEon/images/3872273036-Screenshot%202016-07-30%2015.37.12.png)


### Generated Editor ###

![Screenshot 2016-07-30 15.36.55.png](https://bitbucket.org/repo/nLdEon/images/2242522500-Screenshot%202016-07-30%2015.36.55.png)


### Resulting Inspector ###

![Screenshot 2016-07-30 15.39.23.png](https://bitbucket.org/repo/nLdEon/images/1324551412-Screenshot%202016-07-30%2015.39.23.png)